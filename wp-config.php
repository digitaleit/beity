<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'beitydb');

/** MySQL database username */
define('DB_USER', 'beity');

/** MySQL database password */
define('DB_PASSWORD', 'be1ty');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', 'utf8_unicode_ci'); 

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'waBfPnvRmc1G!iPzOZ;.$c|3(%yc5XmRYO|`i7HfE,z<`Q| N5({T#1o+B`wvk3X');
define('SECURE_AUTH_KEY',  'Z9~YtNgOko/sLH.*hdH4Ak||?zbPHjTc=g?scy|,H]$YFX|mzs7M{?(-:?ZHIqu|');
define('LOGGED_IN_KEY',    'G@,^gm0Y3ySd*ROE^!B#2Vh@|!v)(O1|@Xy,h`~51{Xbp7o-I9s8oR0{Z80|9sXw');
define('NONCE_KEY',        '8xSY]cO-e_JE9Iz?_K-(B8lYwB|i/l4s_6urD2bCU){4,|_-@_vtJKQN-G|;K^X>');
define('AUTH_SALT',        '^xK=4SNW|vW-dC(eKp9z;trUf^o^y%@jr}]r7-3^e}/9T=}Bm%mabAeYl]Q3An-p');
define('SECURE_AUTH_SALT', 'V|U(7uz{V{CD9X8|oo*b6g+vDX)Y+q;6Sh<5mc>4/FF1ofNc|_ Ujy@ BZDN(!0(');
define('LOGGED_IN_SALT',   'Pnh|#`(d6;u4mg;`~ +pE{bX8n?_c(EJh{*<=4+@VggFT+S/N1VXm};ekkA1gI8r');
define('NONCE_SALT',       '?~=,ghAW(Y--S3kdBdc%$9P>aY+)8K>Pc[Db_[k%bQc)N|.iS04X8Yi9Ie%6?C4Y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'digitu_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
