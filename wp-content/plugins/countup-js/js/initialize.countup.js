jQuery(function(){


var options1 = {
  useEasing : setting[0].easing,
  useGrouping : setting[0].grouping,
  separator : setting[0].separator,
  decimal : setting[0].decimal,
  prefix : setting[0].prefix,
  suffix : setting[0].suffix
};

var options2 = {
  useEasing : setting[1].easing,
  useGrouping : setting[1].grouping,
  separator : setting[1].separator,
  decimal : setting[1].decimal,
  prefix : setting[1].prefix,
  suffix : setting[1].suffix
};

var options3 = {
  useEasing : setting[2].easing,
  useGrouping : setting[2].grouping,
  separator : setting[2].separator,
  decimal : setting[2].decimal,
  prefix : setting[2].prefix,
  suffix : setting[2].suffix
};


var demo1 = new CountUp("counterupJSElement1", setting[0].start, setting[0].end, setting[0].decimals, setting[0].duration, options1);
var demo2 = new CountUp("counterupJSElement2", setting[1].start, setting[1].end, setting[1].decimals, setting[1].duration, options2);
var demo3 = new CountUp("counterupJSElement3", setting[2].start, setting[2].end, setting[2].decimals, setting[2].duration, options3);

demo1.start();
demo2.start();
demo3.start();
});
