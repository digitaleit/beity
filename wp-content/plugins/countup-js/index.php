<?php
/*

Plugin Name: Countup JS 
Plugin URI:
Description: Easily incorporate countup.js into your wordpress site through an easy to use shortcode.  CountUp.js is a dependency-free, lightweight JavaScript "class" that can be used to quickly create animations that display numerical data in a more interesting way.  Based off of https://inorganik.github.io/countUp.js/
Author: Barry Ross
Version: 1.4
Author URI: http://www.4d-media.com/plugins
License: The MIT License (MIT)
*/


//Enqueue stylesheet
add_action( 'wp_enqueue_scripts', 'countup_styles_enqueue' );
function countup_styles_enqueue() {
    wp_register_style( 'countup_styles', plugins_url('css/main.css', __FILE__), array(), SBIVER );
    wp_enqueue_style( 'countup_styles' );
}


/* ------------------------------------------------------------------------ *
 * Activation and setup
 * ------------------------------------------------------------------------ */

  function cujswp_activation(){
    $defaultCounterSettings1 = array(
        'fromNumberC1' => 0,
        'toNumberC1' => 0,
        'durationC1' => 0,
        'keyC1' => '',
        'easingC1' => 'true',
        'groupingC1' => 'true',
        'seperatorC1' => ',',
        'decimalC1' => '.',
        'prefixC1' => '',
        'suffixC1' => ''
      );

    $defaultCounterSettings2 = array(
        'fromNumberC2' => 0,
        'toNumberC2' => 0,
        'durationC2' => 0,
        'keyC2' => '',
        'easingC2' => 'true',
        'groupingC2' => 'true',
        'seperatorC2' => ',',
        'decimalC2' => '.',
        'prefixC2' => '',
        'suffixC2' => ''
      );

    $defaultCounterSettings3 = array(
        'fromNumberC3' => 0,
        'toNumberC3' => 0,
        'durationC3' => 0,
        'keyC3' => '',
        'easingC3' => 'true',
        'groupingC3' => 'true',
        'seperatorC3' => ',',
        'decimalC3' => '.',
        'prefixC3' => '',
        'suffixC3' => ''
      );

        $titlesettings = array(
        'Titre-partie-1' => ',',
        'Titre-partie-2' => ','
      );

  add_option('cujswp_options', $titlesettings);
  add_option('cujswp_options', $defaultCounterSettings1);
  add_option('cujswp_options', $defaultCounterSettings2);
  add_option('cujswp_options', $defaultCounterSettings3);


  }

  // Deactivation settings
  function cujswp_deactivation(){
  delete_option('cujswp_options');
  }

  //Scripts
  function cujswp_script() {

    wp_enqueue_script('jquery');
    wp_register_script('countupjs_core', plugins_url('js/countup.js', __FILE__),array("jquery"));
    wp_enqueue_script('countupjs_core');
    wp_register_script('countupjs_initialize', plugins_url('js/initialize.countup.js', __FILE__));
  }

  //activation calls
  register_activation_hook(__FILE__,'cujswp_activation');
  register_deactivation_hook(__FILE__,'cujswp_deactivation');
  add_action('wp_enqueue_scripts', 'cujswp_script');


  /* ------------------------------------------------------------------------ *
   * Menu item and settings pages and registration
   * ------------------------------------------------------------------------ */

  add_action('admin_menu', 'cujswp_page_menu');
  function cujswp_page_menu() {
    add_options_page('Les chifres Beity', 'Les chifres Beity', 'manage_options', 'cujswp-option-page', 'cujswp_option_page_setup');
  }


  //output form, settings and fields sections on plugin's options page
  add_action('admin_init','cujswp_settings_init');
  function cujswp_option_page_setup(){
    ?><div class="wrap">
        <form method="post"action="options.php">
            <h2>Les chifres Beity</h2>
                <?php settings_fields('cujswp-settings4'); ?>
                  <?php do_settings_sections('cujswp-option-page4'); ?>
                  <table><?php do_settings_fields('cujswp-option-page4','cujswp-settings-section4' ); ?></table>
            <table>
              <tr>
                <td>
                  <?php settings_fields('cujswp-settings1'); ?>
                  <?php do_settings_sections('cujswp-option-page1'); ?>
                  <table><?php do_settings_fields('cujswp-option-page1','cujswp-settings-section1' ); ?></table>
                </td>

                <td>
                  <?php settings_fields('cujswp-settings2'); ?>
                  <?php do_settings_sections('cujswp-option-page2'); ?>
                  <table><?php do_settings_fields('cujswp-option-page2','cujswp-settings-section2' ); ?></table>
                </td>

                <td>
                  <?php settings_fields('cujswp-settings3'); ?>
                  <?php do_settings_sections('cujswp-option-page3'); ?>
                  <table><?php do_settings_fields('cujswp-option-page3','cujswp-settings-section3' ); ?></table>
                </td>

              </tr>
              
            </table>
            <?php submit_button(); ?>
        </form>
        </div>
    <?php
  }

  //Initialize settings
  function cujswp_settings_init(){

    //--------------------------
    add_settings_section('cujswp-settings-section1,','Chiffre 1','cujswp_settings_setup','cujswp-option-page1');

    add_settings_field('cujswp-fromnumber','From number ','cujswp_numberfield','cujswp-option-page1','cujswp-settings-section1', array('name'=>'fromNumberC1'));
    add_settings_field('cujswp-tonumber','To number ','cujswp_numberfield','cujswp-option-page1','cujswp-settings-section1', array('name'=>'toNumberC1'));
    add_settings_field('cujswp-duration','Duration ','cujswp_numberfield','cujswp-option-page1','cujswp-settings-section1', array('name'=>'durationC1'));
    add_settings_field('cujswp-prefix','Texte ','cujswp_textfield','cujswp-option-page1','cujswp-settings-section1', array('name'=>'keyC1'));

    // add_settings_field('cujswp-easing','Easing ','cujswp_checkbox','cujswp-option-page1','cujswp-settings-section1', array('name'=>'easingC1'));
    // add_settings_field('cujswp-grouping','Grouping ','cujswp_checkbox','cujswp-option-page1','cujswp-settings-section1', array('name'=>'groupingC1'));
    // add_settings_field('cujswp-serperator','Seperator ','cujswp_textfield','cujswp-option-page1','cujswp-settings-section1', array('name'=>'seperatorC1'));
    // add_settings_field('cujswp-decimal','Decimal ','cujswp_textfield','cujswp-option-page1','cujswp-settings-section1', array('name'=>'decimalC1'));
    // add_settings_field('cujswp-prefix','Prefix ','cujswp_textfield','cujswp-option-page1','cujswp-settings-section1', array('name'=>'prefixC1'));
    // add_settings_field('cujswp-suffix','Suffix ','cujswp_textfield','cujswp-option-page1','cujswp-settings-section1', array('name'=>'suffixC1'));



    //--------------------------
    add_settings_section('cujswp-settings-section2,','Chiffre 2','cujswp_settings_setup','cujswp-option-page2');

    add_settings_field('cujswp-fromnumber','From number ','cujswp_numberfield','cujswp-option-page2','cujswp-settings-section2', array('name'=>'fromNumberC2'));
    add_settings_field('cujswp-tonumber','To number ','cujswp_numberfield','cujswp-option-page2','cujswp-settings-section2', array('name'=>'toNumberC2'));
    add_settings_field('cujswp-duration','Duration ','cujswp_numberfield','cujswp-option-page2','cujswp-settings-section2', array('name'=>'durationC2'));
    add_settings_field('cujswp-prefix','Texte ','cujswp_textfield','cujswp-option-page2','cujswp-settings-section2', array('name'=>'keyC2'));

    // add_settings_field('cujswp-easing','Easing ','cujswp_checkbox','cujswp-option-page2','cujswp-settings-section2', array('name'=>'easingC2'));
    // add_settings_field('cujswp-grouping','Grouping ','cujswp_checkbox','cujswp-option-page2','cujswp-settings-section2', array('name'=>'groupingC2'));
    // add_settings_field('cujswp-serperator','Seperator ','cujswp_textfield','cujswp-option-page2','cujswp-settings-section2', array('name'=>'seperatorC2'));
    // add_settings_field('cujswp-decimal','Decimal ','cujswp_textfield','cujswp-option-page2','cujswp-settings-section2', array('name'=>'decimalC2'));
    // add_settings_field('cujswp-prefix','Prefix ','cujswp_textfield','cujswp-option-page2','cujswp-settings-section2', array('name'=>'prefixC2'));
    // add_settings_field('cujswp-suffix','Suffix ','cujswp_textfield','cujswp-option-page2','cujswp-settings-section2', array('name'=>'suffixC2'));



    //--------------------------
    add_settings_section('cujswp-settings-section3,','Chiffre 3','cujswp_settings_setup','cujswp-option-page3');

    add_settings_field('cujswp-fromnumber','From number ','cujswp_numberfield','cujswp-option-page3','cujswp-settings-section3', array('name'=>'fromNumberC3'));
    add_settings_field('cujswp-tonumber','To number ','cujswp_numberfield','cujswp-option-page3','cujswp-settings-section3', array('name'=>'toNumberC3'));
    add_settings_field('cujswp-duration','Duration ','cujswp_numberfield','cujswp-option-page3','cujswp-settings-section3', array('name'=>'durationC3'));
    add_settings_field('cujswp-prefix','Texte ','cujswp_textfield','cujswp-option-page3','cujswp-settings-section3', array('name'=>'keyC3'));

    // add_settings_field('cujswp-easing','Easing ','cujswp_checkbox','cujswp-option-page3','cujswp-settings-section3', array('name'=>'easingC3'));
    // add_settings_field('cujswp-grouping','Grouping ','cujswp_checkbox','cujswp-option-page3','cujswp-settings-section3', array('name'=>'groupingC3'));
    // add_settings_field('cujswp-serperator','Seperator ','cujswp_textfield','cujswp-option-page3','cujswp-settings-section3', array('name'=>'seperatorC3'));
    // add_settings_field('cujswp-decimal','Decimal ','cujswp_textfield','cujswp-option-page3','cujswp-settings-section3', array('name'=>'decimalC3'));
    // add_settings_field('cujswp-prefix','Prefix ','cujswp_textfield','cujswp-option-page3','cujswp-settings-section3', array('name'=>'prefixC3'));
    // add_settings_field('cujswp-suffix','Suffix ','cujswp_textfield','cujswp-option-page3','cujswp-settings-section3', array('name'=>'suffixC3'));



    add_settings_section('cujswp-settings-section4,','TitreX','cujswp_settings_setup','cujswp-option-page4');

    add_settings_field('cujswp-titre-partie1','Texte partie 1','cujswp_textfield','cujswp-option-page4','cujswp-settings-section4', array('name'=>'Titre-partie-1'));
    add_settings_field('cujswp-titre-partie2','Texte partie 2 ','cujswp_textfield','cujswp-option-page4','cujswp-settings-section4', array('name'=>'Titre-partie-2'));

    register_setting('cujswp-settings1','cujswp_options');
    register_setting('cujswp-settings2','cujswp_options');
    register_setting('cujswp-settings3','cujswp_options');

    register_setting('cujswp-settings4','cujswp_options');


  }

  //Callback from settings setup initialization
  function cujswp_settings_setup(){
  echo "<p>Veuillez configurer vos chiffres</p>";
  }

  //Define textfield output
  function cujswp_textfield($args){
    extract($args);
    $optionArray=(array)get_option('cujswp_options');
    $current_value=$optionArray[$name];
    echo '<input type="text" name="cujswp_options['.$name.']" value="'.$current_value.'"/>'.$explanation.'</br>';
  }

    //Define number output
  function cujswp_numberfield($args){
    extract($args);
    $optionArray=(array)get_option('cujswp_options');
    $current_value=$optionArray[$name];
    echo '<input type="number" name="cujswp_options['.$name.']" value="'.$current_value.'"/>'.$explanation.'</br>';
  }

  //Define checkbox output
  function cujswp_checkbox($args) {
    extract($args);
    $optionArray=(array)get_option('cujswp_options');
    $current_value=$optionArray[$name];
     echo '<input name="cujswp_options['.$name.']" id="cujswp_options['.$name.']" type="checkbox" value="true" class="code" ' . checked( 'true', $current_value, false ) . ' /> ' .$explanation.'</br></br>';
   }

  /* ------------------------------------------------------------------------ *
   * Frontend
   * ------------------------------------------------------------------------ */
  add_shortcode("noschiffres", "cujswp_display");

  function cujswp_display($atts, $content){
    $optionArray=(array)get_option('cujswp_options');
    //Attributes for counter
  	  // $a = shortcode_atts( array(
     //      'startC1' => '5',
     //      'endC1' => '100',
     //      'decimalsC1' => '0',
     //      'durationC1' => '2',
          
     //      'startC2' => '100',
     //      'endC2' => '200',
     //      'decimalsC2' => '0',
     //      'durationC2' => '3',
          
     //      'startC3' => '200',
     //      'endC3' => '300',
     //      'decimalsC3' => '0',
     //      'durationC3' => '5',

     //  ), $atts );


       //   startC1="5" endC1="100" decimalsC1="1" durationC1="5"

    //Pass variable to initialize.countup.js


          $config_array = array(
            array(
            'start'=>$optionArray['fromNumberC1'],
            'end'=>$optionArray['toNumberC1'],
            'decimals'=>$optionArray['decimalsC1'],
            'duration'=>$optionArray['durationC1'],
            'texte'=>$optionArray['keyC1'],
            'easing' => $optionArray['easingC1'],
            'grouping' => $optionArray['groupingC1'],
            'seperator' => $optionArray['seperatorC1'],
            'decimal' => $optionArray['decimalC1'],
            'prefix' => $optionArray['prefixC1'],
            'suffix' => $optionArray['suffixC1']
        ),
            array(
            'start'=>$optionArray['fromNumberC2'],
            'end'=>$optionArray['toNumberC2'],
            'decimals'=>$optionArray['decimalsC2'],
            'duration'=>$optionArray['durationC2'],
            'texte'=>$optionArray['keyC2'],
            'easing' => $optionArray['easingC2'],
            'grouping' => $optionArray['groupingC2'],
            'seperator' => $optionArray['seperatorC2'],
            'decimal' => $optionArray['decimalC2'],
            'prefix' => $optionArray['prefixC2'],
            'suffix' => $optionArray['suffixC2']
        ),
            array(
            'start'=>$optionArray['fromNumberC3'],
            'end'=>$optionArray['toNumberC3'],
            'decimals'=>$optionArray['decimalsC3'],
            'duration'=>$optionArray['durationC3'],
            'texte'=>$optionArray['keyC3'],
            'easing' => $optionArray['easingC3'],
            'grouping' => $optionArray['groupingC3'],
            'seperator' => $optionArray['seperatorC3'],
            'decimal' => $optionArray['decimalC3'],
            'prefix' => $optionArray['prefixC3'],
            'suffix' => $optionArray['suffixC3']
        )

        );

    wp_localize_script('countupjs_initialize', 'setting', $config_array);
    wp_enqueue_script('countupjs_initialize');

	 $output = '
        <div class="chiffre-title"><span class="chiff-blue">'.$optionArray['Titre-partie-1'].'</span><span class="chiff-grey"> '.$optionArray['Titre-partie-2'].'</span></div>
        <div class="chiffre-line"><img class="" src="wp-content/plugins/countup-js/images/LIGNE.png"/></div>
        <ul class="clearfix chiffre-list">

            <li class="chiffre-item">
<div class="spancontainer"><span id="counterupJSElement1" class="countervalue">--</span><br>
<span class="textvalue">'.$optionArray['keyC1'].'</span>    </div>                

            </li>
            
            <li class="chiffre-item">
<div class="spancontainer"><span id="counterupJSElement2" class="countervalue">--</span> <br>
<span class="textvalue">'.$optionArray['keyC2'].'</span>  </div>                                     
            </li>
            
            <li class="chiffre-item">
<div class="spancontainer"><span id="counterupJSElement3" class="countervalue">--</span> <br>
<span class="textvalue">'.$optionArray['keyC3'].'</span>  </div>                                     
            </li>
        
        </ul>

';
   return $output;
}

?>
